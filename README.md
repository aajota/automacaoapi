# Automação de API
**Aplicação para simular a tomada de empréstimo.**


📋 **Pré-requisitos**

Java 8+ JDK deve estar instalado
Maven deve estar instalado e configurado no path da aplicação 


🔧 **Instalação**

Na raiz do projeto, através de seu Prompt de Commando/Terminal/Console execute o
Commando *mvn clean spring-boot:run*
A aplicação estará disponível através da URL _http://localhost:8080_
Você pode trocar a porta da aplicação, caso a 8080 já estiver em uso, adicionando a
propriedade JVM server.port.
_mvn clean spring-boot:run -Dserver.port=8888_ .


⚙️**Executando os testes**

Os testes podem ser executados de forma individual, por classes ou
todos os testes através da classe suite.


🔩 **Para que se aplica os testes**

Os testes validam os seguintes pontos abaixo em uma simulação de tomada de emprestimo. 
*Consulta se um CPF possui ou não restrição.
*Retorna todas as simulações existentes.
*Insere uma nova simulação.
*Retorna uma simulação através do CPF.
*Atualiza uma simulação existente através do CPF*
*Remove uma simulação existente através do CPF.


🛠️ **Construído com**

Linguagem de programação JAVA.
Maven 
Dependencies (REST Assured, Junit, gson).
IDE Eclipse.


🐞 **Erros encontrados**

**Retorno esperado** 
Uma simulação para um mesmo CPF retorna um HTTP Status _409_ com a mensagem "CPF já existente" 
**Retorno obtido* *
Está retornando status _400_

**Retorno esperado**
Retorna o HTTP Status _204_ se simulação for removida com sucesso
**Retorno obtido** 
Está retornando status _200_

**Retorno esperado** 
Retorna o HTTP Status _404_ com a mensagem "Simulação não encontrada" se não
existir a simulação pelo ID informado.
**Retorno esperado** 
Está retornando status _200_.


