package br.com.desafioApi;

import static io.restassured.RestAssured.given;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import utils.Base;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DeletarSimulacao extends Base{
	
	@Test
	public void t11_removerSimulacao() {
		given()
		.when()
		   .delete("simulacoes/11")
		.then()
		   .statusCode(200);
	}
	
	@Test
	public void t12_simulacaoNaoEncontrada() {
		given()
		.when()
		   .delete("simulacoes/37892") 
		.then()
		   .statusCode(200);
	}
}
