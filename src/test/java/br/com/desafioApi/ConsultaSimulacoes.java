package br.com.desafioApi;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import utils.Base;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ConsultaSimulacoes extends Base{
	
	@Test
	public void t08_consultarTodasSimulacao() {
		given()
		.when()
		   .get("simulacoes")
		.then()
		   .statusCode(200)
		;
	}
	@Test
	public void t09_simulacaoCpfCadastrado() {
		given()
		.when()
		   .get("simulacoes/45571535008")
		.then()
		   .statusCode(200);
	}
	@Test
	public void t10_simulacaoCpfNaoCadastrada() {
		given()
		.when()
		   .get("simulacoes/59484444067")
		.then()
		   .statusCode(404)
		   .body("mensagem", is("CPF 59484444067 n�o encontrado"));
	}
}
