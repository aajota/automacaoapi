package br.com.desafioApi;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import utils.Base;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AlterarCadastro extends Base{

	@Test
	public void t06_alterarCadastro() {
		given()
		   .body("{\"id\": 12, \"nome\": \"Deltrano\", \"cpf\": \"17822386034\", \"email\": \"ronaldo@gmail.com\", \"valor\": 200,\"parcelas\": 5, \"seguro\": false}")
		.when()
		   .put("simulacoes/17822386034")
		.then()
		   .statusCode(200);
	}
	
	@Test
	public void t07_cpfNaoCadastrado() {
		given()
		   .body("{\"id\": 12, \"nome\": \"Deltrano\", \"cpf\": \"17822386034\", \"email\": \"ronaldo@gmail.com\", \"valor\": 200,\"parcelas\": 5, \"seguro\": false}")
		.when()
		   .put("simulacoes/08338271039")
		.then()
		   .statusCode(404)
		   .body("mensagem", is("CPF 08338271039 n�o encontrado"));
	}
}
