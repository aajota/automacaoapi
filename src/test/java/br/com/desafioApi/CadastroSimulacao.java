package br.com.desafioApi;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import utils.Base;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CadastroSimulacao extends Base{

	@Test
	public void t03_simulacaoCadastroComSucesso() {
		given()
		   .body("{ \"nome\": \"Antonio Goes\", \"cpf\": 45571535008, \"email\": \"antonio@simulacao.com\", \"valor\": 7500, \"parcelas\": 5, \"seguro\": true}")
		.when()
		   .post("simulacoes")
		.then()
		   .statusCode(201);  
	}
	
	@Test
	public void t04_simulacaoCadastroComErro() {
		given()
		   .body("{ \"nome\": \"Antonio Goes\", \"cpf\":, \"email\": \"antonio@simulacao.com\", \"valor\": 7500, \"parcelas\": 5, \"seguro\": true}")
		.when()
		   .post("simulacoes")
		.then()
		   .statusCode(400);
	}

	@Test
	public void t05_simulacaoCpfDuplicado() { 
		given()
		   .body("{ \"nome\": \"Antonio Goes\", \"cpf\": 45571535008, \"email\": \"antonio@simulacao.com\", \"valor\": 7500, \"parcelas\": 5, \"seguro\": true}")
		.when()
		   .post("simulacoes")
		.then()
		   .statusCode(400)
		   .body("mensagem", is("CPF duplicado"));
	}
}
