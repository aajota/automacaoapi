package br.com.desafioApi;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import utils.Base;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ConsultarRestricaoCpf extends Base{

	@Test
	public void t01_consultarCpfComRestricao() {
		given()
		.when()
		   .get("restricoes/58063164083")
		.then()
		   .statusCode(200)
		   .body("mensagem", is("O CPF 58063164083 tem problema"));
	}
	
	@Test
	public void t02_consultarCpfSemRestricao() {
		given()
		.when()
		   .get("restricoes/45571535008")
		.then()
		   .statusCode(204);
	}
}
