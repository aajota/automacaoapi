package suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite.SuiteClasses;
import br.com.desafioApi.AlterarCadastro;
import br.com.desafioApi.CadastroSimulacao;
import br.com.desafioApi.ConsultaSimulacoes;
import br.com.desafioApi.ConsultarRestricaoCpf;
import br.com.desafioApi.DeletarSimulacao;

@RunWith(org.junit.runners.Suite.class)
@SuiteClasses({
	ConsultarRestricaoCpf.class,
	CadastroSimulacao.class,
	AlterarCadastro.class,
	ConsultaSimulacoes.class,
	DeletarSimulacao.class,
	
})

public class Suite {
	
}
